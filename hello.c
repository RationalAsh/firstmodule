/* This is my first ever kernel module written in C*/
/*
    I'll write up some useful information here so that I can refer back to this
    when I want to. Every kernel module needs to start out by including the three
    header files I've included below. The init.h is actually not necessary all the
    time. It's only necessary if we want to define the module init and module exit
    functions with names that are non standard. The init.h header contains macros
    that make sure that the correct functions are passed on.

    Every kernel module needs an init and an exit function. The init function is
    called when the module is loaded and the exit function is called when the module
    is unloaded. This used to be done by defining the init_module() and cleanup_module()
    functions. But now it can be done by defining the functions with any name I want
    and then at the end passing these function names to the module_init() and
    module_exit() functions.

    The printk function prints log messages at different levels. There is a table of
    log levels on the internet. I think I'll download the table and save it somewhere
    for easy reference. Or maybe even better. I'll write this up as a blogpost but publish
    it under a private blog so that people don't copy my answer and submit it as their own.

    The list of log levels:
    KERN_EMERG   - Emergency messages that precede a crash
    KERN_ALERT   - Error requiring immediate attention
    KERN_CRIT    - Critical error (Hardware or software)
    KERN_ERR     - Error conditions (common in drivers)
    KERN_WARNING - Warning conditions (could lead to errors)
    KERN_NOTICE  - Not an error but a significant condition
    KERN_INFO    - Informational message
    KERN_DEBUG   - Used only for debug messages
    KERN_DEFAULT - Default logging level
    KERN_CONT    - Continuation of a log line (avoid adding new time stamp)

    How to load the module into the kernel:
    $ insmod <filename>.ko
    $ dmesg|tail

    How to unload the module from the kernel:
    $ rmmod hello.ko
    $ dmesg|tail
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#define DRIVER_AUTHOR "Ashwin Narayan"
#define DRIVER_DESC "Prints hello world to log level"

//Definition of the init function
static int __init hello_init(void)
{
    printk(KERN_DEBUG "Hello World!\n");
    
    return 0;
}

//Definition of the exit(cleanup) function.
static void __exit hello_exit(void)
{
    printk(KERN_DEBUG "Goodbye Cruel World!\n");
}


module_init(hello_init);
module_exit(hello_exit);

//Macro to declare the license of the module
MODULE_LICENSE("GPL");
//Macro to declare the module author
MODULE_AUTHOR(DRIVER_AUTHOR);
//Macro to declare a string describing the module
MODULE_DESCRIPTION(DRIVER_DESC);
